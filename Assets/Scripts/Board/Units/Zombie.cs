﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : Unit {

    public override IEnumerator MakeTurn() {
        Hex[] path = FindPath(BoardManager.instance.GetPlayer().hex);
        
        Hex[] actualPath;
        Unit unitToAttack;
        AnalyzePath(path, out unitToAttack, out actualPath);

        ChangePathHighlighting(path, true);
        yield return base.Move(actualPath);
        ChangePathHighlighting(path, false);

        if (unitToAttack == null) { // try to get player in attack range after moving
            Unit[] unitsInAttackArea = BoardManager.instance.GetUnitsInAttackArea(hex, attackType, range, UnitType.PLAYER);
            if (unitsInAttackArea.Length > 0) {
                unitToAttack = unitsInAttackArea[0];
            }
        }
        if (unitToAttack != null) {
            FlipSpriteIfNeeded(hex, unitToAttack.hex);
            BoardOutputManager.instance.OnZombieAttacks();
            unitToAttack.TakeDamage(damage);
        }
        yield return base.WaitForTurnEndDelay();
    }

    public override void TakeDamage(int damage) {
        health -= damage;
        if (health <= 0) {
            BoardManager.instance.OnUnitDies(this);
            enabled = false;
            Destroy(gameObject);
        }
    }

}
