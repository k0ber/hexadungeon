﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Random = UnityEngine.Random;

public class Archer : Unit {

    private int nonAttackRange = 1;

    public override IEnumerator MakeTurn() {
        // attack if player in attack range
        Unit targetPlayer = BoardManager.instance.GetPlayer();
        List<List<Hex>> freeAttackDiagonals = BoardManager.instance.GetFreeDiagonals(this, targetPlayer, this.hex, range, nonAttackRange, new Hex.Type[] { Hex.Type.DEFAULT, Hex.Type.HOLE });
        Unit player = FindPlayer(freeAttackDiagonals);
        if (player != null) {
            yield return RangeAttack(player);
            yield return WaitForTurnEndDelay();
            yield break;
        }

        // find path to closest player diagonal
        List<List<Hex>> freePlayerDiagonals = BoardManager.instance.GetFreeDiagonals(this, targetPlayer, targetPlayer.hex, range, nonAttackRange, new Hex.Type[] { Hex.Type.DEFAULT, Hex.Type.HOLE });
        foreach (List<Hex> diagonal in freePlayerDiagonals) { // remove hexes we can't move to
            diagonal.RemoveAll(hex => hex.hexType != Hex.Type.DEFAULT);
        }
        List<Hex> lastFreeDiagonalHexes = new List<Hex>();
        foreach (List<Hex> freeDiagonal in freePlayerDiagonals) {
            if (freeDiagonal.Count > 0) {
                lastFreeDiagonalHexes.Add(freeDiagonal[freeDiagonal.Count - 1]);
            }
        }
        // we can't find any free diagonals
        if (lastFreeDiagonalHexes.Count == 0) {
            // move to random position?
            yield break;
        }

        // free diagonal is exist
        Dictionary<float, List<Hex>> distancesToLastHexes = new Dictionary<float, List<Hex>>();
        foreach (Hex lastDiagonalHex in lastFreeDiagonalHexes) {
            float distanceToLastHex = Hex.Distance(hex, lastDiagonalHex);
            //Debug.Log(string.Format("Archer find distance to [{0}, {1}] is {2}", lastDiagonalHex.Q, lastDiagonalHex.R, distanceToLastHex));
            List<Hex> distancesHexes;
            distancesToLastHexes.TryGetValue(distanceToLastHex, out distancesHexes);
            if (distancesHexes != null) { // we're already have list with same distance
                distancesHexes.Add(lastDiagonalHex);
            } else {
                distancesHexes = new List<Hex>();
                distancesHexes.Add(lastDiagonalHex);
                distancesToLastHexes.Add(distanceToLastHex, distancesHexes);
            }
        }
        List<float> distances = new List<float>();
        foreach (float distance in distancesToLastHexes.Keys) {
            distances.Add(distance);
        }
        float minDistance = distances.Min();
        List<Hex> closestLastDiagonalHexes;
        distancesToLastHexes.TryGetValue(minDistance, out closestLastDiagonalHexes);

        int randomHexIndex = Random.Range(0, closestLastDiagonalHexes.Count - 1);
        Hex hexToMove = closestLastDiagonalHexes[randomHexIndex];
        //Debug.Log(string.Format("Archer desides to [{0}, {1}]", hexToMove.Q, hexToMove.R));
        Hex[] path = FindPath(hexToMove);

        ChangePathHighlighting(path, true);
        yield return base.Move(path);
        ChangePathHighlighting(path, false);
    }

    private Unit FindPlayer(List<List<Hex>> freeDiagonals) {
        foreach (List<Hex> diagonal in freeDiagonals) {
            foreach (Hex diagonalHex in diagonal) {
                Unit unit = diagonalHex.unit;
                if (unit != null && unit.unitType == UnitType.PLAYER) {
                    return diagonalHex.unit;
                }
            }
        }
        return null;
    }

    private IEnumerator RangeAttack(Unit unit) {
        BoardOutputManager.instance.OnArcherAttacks();
        // play shooting animation
        GameObject projectile = BoardFabric.instance.GetProjectileInstance(this);
        yield return projectile.GetComponent<Projectile>().MoveTo(unit.transform.position);
        Destroy(projectile);
        unit.TakeDamage(damage);
    }

    public override void TakeDamage(int damage) {
        health -= damage;
        if (health <= 0) {
            BoardManager.instance.OnUnitDies(this);
            enabled = false;
            Destroy(gameObject);
        }
    }

}
