﻿using QPath;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Unit : MonoBehaviour, IQPathUnit {

    private static readonly float TURN_END_DELAY = 0.2f;
    private static readonly float ONE_STEP_MOVE_TIME = 0.2f;  //Time it will take object to move, in seconds.

    public enum UnitType { PLAYER, ENEMY }
    public enum AttackType { CIRCLE, SPACED_DIAGONALS }

    public UnitType unitType;
    public int health { get; protected set; }
    public int speed { get; protected set; }
    public AttackType attackType { get; protected set; }
    public int range { get; protected set; }
    public int damage { get; protected set; }
    public Hex hex { get; protected set; }

    private Rigidbody2D rb2D;
    private SpriteRenderer spRenderer;
    private float inverseMoveTime; //Used to make movement more efficient.
    private SightDirection sightDirection;

    public void Init(UnitType unitType, Hex hex, int health, int speed, AttackType attackType, int range, int damage) {
        this.unitType = unitType;
        this.hex = hex;
        this.health = health;
        this.speed = speed;
        this.attackType = attackType;
        this.range = range;
        this.damage = damage;
        sightDirection = SightDirection.LEFT; // dependes on assets, means spRenderer.flipX = false
    }

    protected virtual void Start() {
        rb2D = GetComponent<Rigidbody2D>();
        spRenderer = GetComponent<SpriteRenderer>();
        inverseMoveTime = 1f / ONE_STEP_MOVE_TIME; //By storing the reciprocal of the move time we can use it by multiplying instead of dividing, this is more efficient.
        hex.unit = this;
    }

    public abstract IEnumerator MakeTurn();

    public abstract void TakeDamage(int damage);

    public IEnumerator Move(Hex[] path) {
        Hex initialHex = this.hex;
        Hex[] steps = SafeSubArray(path, 1, speed); // ignore first hex, we are already here, don't need to move
        foreach (Hex stepHex in steps) {
            Vector2 stepDestination = BoardManager.instance.GetGameObjectFromHex(stepHex).transform.position;
            FlipSpriteIfNeeded(hex, stepHex);
            yield return SmoothMovement(stepDestination);
            hex.unit = null;
            hex = stepHex;
            hex.unit = this;
        }
    }

    protected internal IEnumerator WaitForTurnEndDelay() {
        yield return new WaitForSeconds(TURN_END_DELAY);
    }

    // TODO: try Vector3.SmoothDamp instead of Vector3.MoveTowards
    protected IEnumerator SmoothMovement(Vector3 end) {
        //Calculate the remaining distance to move based on the square magnitude of the difference between current position and end parameter. 
        //Square magnitude is used instead of magnitude because it's computationally cheaper.
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        //While that distance is greater than a very small amount (Epsilon, almost zero):
        while (sqrRemainingDistance > float.Epsilon) {
            //Find a new position proportionally closer to the end, based on the moveTime
            Vector3 newPostion = Vector3.MoveTowards(rb2D.position, end, inverseMoveTime * Time.deltaTime);
            //Call MovePosition on attached Rigidbody2D and move it to the calculated position.
            rb2D.MovePosition(newPostion);
            //Recalculate the remaining distance after moving.
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null; //Return and loop until sqrRemainingDistance is close enough to zero to end the function
        }
    }

    // works only for neighbors hexes
    protected void FlipSpriteIfNeeded(Hex from, Hex to) {
        Pair<int> changedDirection = new Pair<int>(to.Q - from.Q, to.R - from.R);
        if (new List<Pair<int>>(Hex.LeftDirections).Contains(changedDirection) && sightDirection == SightDirection.RIGHT) {
            spRenderer.flipX = false;
            sightDirection = SightDirection.LEFT;
            return;
        }
        if (new List<Pair<int>>(Hex.RightDirections).Contains(changedDirection) && sightDirection == SightDirection.LEFT) {
            spRenderer.flipX = true;
            sightDirection = SightDirection.RIGHT;
            return;
        }
    }

    protected void AnalyzePath(Hex[] rawPath, out Unit unitToAttack, out Hex[] actualPath) {
        if (rawPath.Length == 0) {
            actualPath = new Hex[0];
            unitToAttack = null;
            return;
        }
        Unit unitInLastHex = rawPath[rawPath.Length - 1].unit;
        bool canAttack = unitInLastHex != null && rawPath.Length - 1 <= speed;
        actualPath = canAttack ? actualPath = SafeSubArray(rawPath, 0, rawPath.Length - 1) : rawPath;
        unitToAttack = canAttack ? unitInLastHex : null;
    }

    protected Hex[] FindPath(Hex destinationHex) {
        return QPath.QPath.FindPath<Hex>(this, this.hex, destinationHex, Hex.CostEstimate);
    }

    public float CostToEnterHex(IQPathTile sourceTile, IQPathTile destinationTile) {
        return 1;
    }

    public float AggregateTurnsToEnterHex(Hex hex, float turnsToDate) {
        return 1;
    }

    public static T[] SafeSubArray<T>(T[] data, int index, int length) {
        int slicedLingth = data.Length - index;
        int actualLength = slicedLingth < length ? slicedLingth : length;
        if (actualLength <= 0) {
            return new T[0];
        }
        T[] result = new T[actualLength];
        Array.Copy(data, index, result, 0, actualLength);
        return result;
    }

    protected void ChangePathHighlighting(Hex[] path, bool highlight) {
        foreach (Hex hexStep in path) {
            BoardManager.instance.GetGameObjectFromHex(hexStep).GetComponent<SpriteRenderer>().color = highlight ? Color.cyan : Color.white;
        }
    }

}
