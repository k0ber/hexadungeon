﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardOutputManager : MonoBehaviour {

    public static BoardOutputManager instance;

    //public AudioManager audioManager;
    //public UIManager uiManager;
    public AudioClip playerAttackSound;
    public AudioClip zombieAttackSound;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void OnPlayerAttacks() {
        AudioManager.instance.PlayRandomizedSfx(playerAttackSound);
    }

    public void OnZombieAttacks() {
        AudioManager.instance.PlayRandomizedSfx(zombieAttackSound);
    }

    public void OnArcherAttacks() {
        AudioManager.instance.PlayRandomizedSfx(zombieAttackSound);
    }

    public void OnGameOver() {
        AudioManager.instance.musicSource.Stop();
    }

}
