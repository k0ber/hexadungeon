﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using QPath;
using System;

/// <summary>
/// https://www.redblobgames.com/grids/hexagons/
/// </summary>
public class Hex : IQPathTile {

    public enum Type {
        DEFAULT,
        OBSTACLE,
        HOLE
    }

    public Hex(int q, int r) {
        this.Q = q;
        this.R = r;
        this.S = -(q + r);
    }

    public Unit unit;
    public Type hexType;

    public readonly int Q;  // Column
    public readonly int R;  // Row
    public readonly int S;

    public static Pair<int>[] DiagonalDirections {
        get {return new Pair<int>[] {new Pair<int>(0, 1), new Pair<int>(1, 1), new Pair<int>(1, 0), new Pair<int>(0, -1), new Pair<int>(-1, -1), new Pair<int>(-1, 0)};}
    }

    public static Pair<int>[] LeftDirections {
        get { return new Pair<int>[] { new Pair<int>(0, 1), new Pair<int>(-1, 0), new Pair<int>(-1, -1) }; }
    }

    public static Pair<int>[] RightDirections {
        get { return new Pair<int>[] { new Pair<int>(1, 1), new Pair<int>(1, 0), new Pair<int>(0, -1) }; }
    }

    static readonly float WIDTH_MULTIPLIER = Mathf.Sqrt(3) / 2;
    static readonly float RADIUS = 1f;

    public override string ToString() {
        return Q + ", " + R;
    }

    // Returns the world-space position of this hex
    public Vector2 Position() {
        float xPos = HexHorizontalSpacing() * (this.Q - (this.R / 2f));
        float yPos = HexVerticalSpacing() * this.R;
        return new Vector2(xPos, yPos);
    }

    public float HexHeight() {
        return RADIUS * 2;
    }

    public float HexWidth() {
        return WIDTH_MULTIPLIER * HexHeight();
    }

    public float HexVerticalSpacing() {
        return HexHeight() * 0.75f;
    }

    public float HexHorizontalSpacing() {
        return HexWidth();
    }

    public static float CostEstimate(IQPathTile aa, IQPathTile bb) {
        return Distance((Hex)aa, (Hex)bb);
    }

    public static float Distance(Hex a, Hex b) {
        int dQ = a.Q - b.Q;
        int dR = a.R - b.R;
        int dD = dQ - dR;
        return Mathf.Max(Mathf.Abs(dQ), Mathf.Abs(dR), Mathf.Abs(dD));
    }

    public Hex[] GetArea(int distance) {
        List<Hex> result = new List<Hex>();
        for (int dx = -distance; dx <= distance; dx++) {
            for (int dy = Mathf.Max(-distance, -dx - distance); dy <= Mathf.Min(distance, -dx + distance); dy++) {
                Utils.AddIfNotNull(result, BoardManager.instance.GetHexAt(Q - dx, R + dy));
            }
        }
        return result.ToArray();
    }

    public static int GetActualColumnForSquareField(int column, int row) {
        return column + (int)Mathf.Floor(row / 2);
    }

    #region IQPathTile implementation
    public IQPathTile[] GetNeighbours(IQPathTile endTile) {
        Unit targetUnit = ((Hex)endTile).unit;
        List<Hex> neighbours = new List<Hex>();
        foreach (Pair<int> direction in DiagonalDirections) {
            Utils.AddIfNotNull(neighbours, BoardManager.instance.GetEmptyHexAt(unit, targetUnit, Q + direction.first, R + direction.second, new Hex.Type[] { Hex.Type.DEFAULT }));
        }
        return neighbours.ToArray();
    }

    public float AggregateCostToEnter(float costSoFar, IQPathTile sourceTile, IQPathUnit theUnit) {
        return ((Unit)theUnit).AggregateTurnsToEnterHex(this, costSoFar);
    }
    #endregion
}
