﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


public class LayerOrderManager : MonoBehaviour {

    private static int MAX_LEVEL = 100;
    private SortingGroup sortingGroup;

    void Start() {
        sortingGroup = GetComponent<SortingGroup>();
    }

    void Update() {
        if (transform.hasChanged) {
            transform.hasChanged = false;
            sortingGroup.sortingOrder = Mathf.RoundToInt(MAX_LEVEL - transform.position.y);
        }
    }

}
