﻿using UnityEngine;

public class BoardFabric : MonoBehaviour {

    public static BoardFabric instance;

    public GameObject playerPrefab;
    public GameObject zombiePrefab;
    public GameObject archerPrefab;
    public GameObject hexPrefab;
    public GameObject projectilePrefab;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }


    public void PlayerInstance(Hex hex, MonoBehaviour parent, out GameObject playerGameObject, out Unit playerUnit) {
        playerGameObject = Instantiate(playerPrefab, hex.Position(), Quaternion.identity, parent.transform);
        playerUnit = playerGameObject.GetComponent<Player>();
        playerUnit.Init(Unit.UnitType.PLAYER, hex, 10, 1, Unit.AttackType.CIRCLE, 1, 1);
    }

    public void ZombieInstance(Hex hex, MonoBehaviour parent, out GameObject zombieGameObject, out Unit zombieUnit) {
        zombieGameObject = Instantiate(zombiePrefab, hex.Position(), Quaternion.identity, parent.transform);
        zombieUnit = zombieGameObject.GetComponent<Zombie>();
        zombieUnit.Init(Unit.UnitType.ENEMY, hex, 1, 1, Unit.AttackType.CIRCLE, 1, 1);
    }

    public void ArcherInstance(Hex hex, MonoBehaviour parent, out GameObject archerGameObject, out Unit archerUnit) {
        archerGameObject = Instantiate(archerPrefab, hex.Position(), Quaternion.identity, parent.transform);
        archerGameObject.GetComponent<SpriteRenderer>().color = Color.green;
        archerUnit = archerGameObject.GetComponent<Archer>();
        archerUnit.Init(Unit.UnitType.ENEMY, hex, 1, 1, Unit.AttackType.SPACED_DIAGONALS, 5, 1);
    }

    public GameObject HexInstance(Hex hex, MonoBehaviour parent) {
        return Instantiate(hexPrefab, hex.Position(), Quaternion.identity, parent.transform);
    }

    public GameObject GetProjectileInstance(MonoBehaviour parent) {
        return Instantiate(projectilePrefab, parent.transform.position, Quaternion.identity, parent.transform);
    }

}
