﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshText : MonoBehaviour {

    void Start() {
        var parent = transform.parent;

        var parentRenderer = parent.GetComponent<Renderer>();
        var renderer = GetComponent<Renderer>();
        renderer.sortingLayerID = parentRenderer.sortingLayerID;
        renderer.sortingOrder = parentRenderer.sortingOrder;

        var spriteTransform = parent.transform;
        var text = GetComponent<TextMesh>();
        var pos = spriteTransform.position;

        Hex hex = BoardManager.instance.GetHexFromGameObject(parent.gameObject);
        text.text = string.Format("[" + hex.Q + ", " + hex.R + "]");
    }

}
