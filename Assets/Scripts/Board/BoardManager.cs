﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

    public static BoardManager instance;

    public int numColumns;
    public int numRows;

    private Dictionary<Pair<int>, Hex> hexes;
    private Dictionary<Hex, GameObject> hexToGameObjectMap;
    private Dictionary<GameObject, Hex> gameObjectToHexMap;

    private List<Unit> units;
    private Dictionary<Unit, GameObject> unitToGameObjectMap;
    private Dictionary<GameObject, Unit> gameObjectToUnitMap;

    private Player player;
    private bool isGameOver;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }

    public void GenerateBoardAndStartTurns() { // todo: add difficulty
        hexes = new Dictionary<Pair<int>, Hex>();
        hexToGameObjectMap = new Dictionary<Hex, GameObject>();
        gameObjectToHexMap = new Dictionary<GameObject, Hex>();


        for (int column = 0; column < numColumns; column++) {
            for (int row = 0; row < numRows; row++) {
                int actualColumn = Hex.GetActualColumnForSquareField(column, row);
                int actualRow = row;
                //Debug.Log(string.Format("QR [{0}, {1}] ==> [{2}, {3}]", column, row, actualColumn, actualRow));

                Hex hex = new Hex(actualColumn, actualRow);
                hexes.Add(new Pair<int>(actualColumn, actualRow), hex);

                GameObject hexGO = BoardFabric.instance.HexInstance(hex, this);
                hexToGameObjectMap[hex] = hexGO;
                gameObjectToHexMap[hexGO] = hex;

                hexGO.name = string.Format("HEX: {0},{1}", actualColumn, actualRow);
            }
        }

        // TODO: random generation
        units = new List<Unit>();
        unitToGameObjectMap = new Dictionary<Unit, GameObject>();
        gameObjectToUnitMap = new Dictionary<GameObject, Unit>();

        GameObject unitGO;
        Unit unit;

        Hex playerHex1 = hexes[new Pair<int>(3, 5)];
        BoardFabric.instance.PlayerInstance(playerHex1, this, out unitGO, out unit);
        units.Add(unit);
        unitToGameObjectMap.Add(unit, unitGO);
        gameObjectToUnitMap.Add(unitGO, unit);
        player = unitGO.GetComponent<Player>();

        //Hex enemyHex1 = hexes[new Pair<int>(10, 7)];
        //BoardFabric.instance.ZombieInstance(enemyHex1, this, out unitGO, out unit);
        //units.Add(unit);
        //unitToGameObjectMap.Add(unit, unitGO);
        //gameObjectToUnitMap.Add(unitGO, unit);

        //Hex enemyHex2 = hexes[new Pair<int>(9, 6)];
        //BoardFabric.instance.ZombieInstance(enemyHex2, this, out unitGO, out unit);
        //units.Add(unit);
        //unitToGameObjectMap.Add(unit, unitGO);
        //gameObjectToUnitMap.Add(unitGO, unit);

        //Hex enemyHex4 = hexes[new Pair<int>(7, 4)];
        //BoardFabric.instance.ZombieInstance(enemyHex4, this, out unitGO, out unit);
        //units.Add(unit);
        //unitToGameObjectMap.Add(unit, unitGO);
        //gameObjectToUnitMap.Add(unitGO, unit);

        Hex enemyHex3 = hexes[new Pair<int>(9, 9)];
        BoardFabric.instance.ArcherInstance(enemyHex3, this, out unitGO, out unit);
        units.Add(unit);
        unitToGameObjectMap.Add(unit, unitGO);
        gameObjectToUnitMap.Add(unitGO, unit);

        //Hex enemyHex5 = hexes[new Pair<int>(4, 1)];
        //BoardFabric.instance.ArcherInstance(enemyHex5, this, out unitGO, out unit);
        //units.Add(unit);
        //unitToGameObjectMap.Add(unit, unitGO);
        //gameObjectToUnitMap.Add(unitGO, unit);

        MakeSpecialHexAt(2, 1, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(2, 2, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(5, 4, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(5, 3, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(8, 6, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(9, 7, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(8, 4, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(9, 4, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(10, 8, Hex.Type.OBSTACLE);
        MakeSpecialHexAt(4, 4, Hex.Type.HOLE);
        MakeSpecialHexAt(6, 7, Hex.Type.HOLE);
        MakeSpecialHexAt(7, 7, Hex.Type.HOLE);

        isGameOver = false;
        StartCoroutine(TurnUnits());
    }

    private void MakeSpecialHexAt(int Q, int R, Hex.Type hexType) {
        Hex obstacleHex1 = GetHexAt(Q, R);
        obstacleHex1.hexType = hexType;
        hexToGameObjectMap[obstacleHex1].GetComponent<SpriteRenderer>().color = 
            (hexType == Hex.Type.OBSTACLE) ? Color.yellow : Color.red;
    }

    private IEnumerator TurnUnits() {
        while (!isGameOver) {
            foreach (Unit unit in units) {
                if (unit != null && unit.isActiveAndEnabled) {
                    yield return StartCoroutine(unit.MakeTurn());
                }
            }
        }
    }

    public void OnGameOver() {
        isGameOver = true;
    }

    public void OnUnitDies(Unit diedUnit) {
        diedUnit.enabled = false;
        List<Unit> aliveUnits = new List<Unit>();
        foreach (Unit unit in units) {
            if (unit != null && unit.enabled) {
                aliveUnits.Add(unit);
            }
        }
        if (aliveUnits.Count == 1 && aliveUnits[0].unitType == Unit.UnitType.PLAYER) {
            GameManager.instance.ExitFromRoom();
        }
    }

    public Unit[] GetUnitsInAttackArea(Hex initialHex, Unit.AttackType attackType, int range, Unit.UnitType unitTypeToAttack) {
        List<Unit> result = new List<Unit>();
        Hex[] area = new Hex[0];
        if (attackType == Unit.AttackType.CIRCLE) {
            area = initialHex.GetArea(range);
        }
        foreach (Hex areaHex in area) {
            Unit unit = areaHex.unit;
            if (unit != null && unit.unitType == unitTypeToAttack) {
                result.Add(unit);
            }
        }
        return result.ToArray();
    }

    public List<List<Hex>> GetFreeDiagonals(Unit initialUnit, Unit targetUnit, Hex initialHex, int diagonalLength, int startSpace, Hex.Type[] allowedHexTypes) {
        List<List<Hex>> result = new List<List<Hex>>();
        foreach (Pair<int> diagonalDirection in Hex.DiagonalDirections) {
            List<Hex> diagonaHexes = new List<Hex>();
            for (int i = 1 + startSpace; i < diagonalLength; i++) {
                Hex emptyDiagonalHex = GetEmptyHexAt(initialUnit, targetUnit, initialHex.Q + diagonalDirection.first * i, initialHex.R + diagonalDirection.second * i, allowedHexTypes);
                if (emptyDiagonalHex != null) {
                    diagonaHexes.Add(emptyDiagonalHex);
                } else {
                    break;
                }
            }
            if (diagonaHexes.Count > 0) {
                result.Add(diagonaHexes);
            }
        }
        return result;
    }

    public Hex GetHexAt(int Q, int R) {
        var key = new Pair<int>(Q, R);
        if (hexes.ContainsKey(key)) {
            return hexes[key];
        } else {
            return null;
        }
    }

    public Hex GetEmptyHexAt(Unit movingUnit, Unit targetUnit, int Q, int R, Hex.Type[] allowedHexTypes) {
        Hex hex = GetHexAt(Q, R);
        bool isHexAllowed = hex != null && new List<Hex.Type>(allowedHexTypes).Contains(hex.hexType);
        bool isNoBlockingUnitsOnHex = hex != null && (hex.unit == null || hex.unit == movingUnit || hex.unit == targetUnit);
        return isHexAllowed && isNoBlockingUnitsOnHex ? hex : null;
    }

    public Player GetPlayer() {
        return player;
    }

    public Hex GetHexFromGameObject(GameObject hexGO) {
        if (gameObjectToHexMap.ContainsKey(hexGO)) {
            return gameObjectToHexMap[hexGO];
        }
        return null;
    }

    public GameObject GetGameObjectFromHex(Hex hex) {
        if (hexToGameObjectMap.ContainsKey(hex)) {
            return hexToGameObjectMap[hex];
        }
        return null;
    }

}