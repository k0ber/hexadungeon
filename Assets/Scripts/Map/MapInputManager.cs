﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInputManager : MonoBehaviour {

    public static MapInputManager instance;

    public LayerMask layerIDForRooms;
    public delegate void OnRoomClicked(Room room);
    public event OnRoomClicked RoomClickedListeners;


    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Room room = GetRoomUnderMouse();
            if (room!= null) {
                //Debug.Log("Clicked hex " + hex.Q + " " + hex.R);
                RoomClickedListeners.Invoke(room);
            } else {
                // nothing clicked
            }
        }
    }

    private Room GetRoomUnderMouse() {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePosition2D = new Vector2(mousePosition.x, mousePosition.y);
        RaycastHit2D hit = Physics2D.Raycast(mousePosition2D, Vector2.zero, layerIDForRooms.value);
        if (hit.collider != null) {
            GameObject roomGO = hit.collider.gameObject; // The collider is a child of the "correct" game object that we want.
            return MapManager.instance.GetRoomFromGameObject(roomGO);
        }
        return null;
    }

}
