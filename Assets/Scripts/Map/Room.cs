﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room {

    public readonly float x;
    public readonly float y;
    public List<Room> neighbours;
    public RoomType roomType;

    public Room(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public bool IsInRange(Room otherRoom, float radius) {
        return Mathf.Pow(this.x - otherRoom.x, 2) + Mathf.Pow(this.y - otherRoom.y, 2) <= Mathf.Pow(radius, 2);
    }

    public bool HasNeighbour(Room room) {
        if (neighbours == null) {
            return false;
        }
        return neighbours.Contains(room);
    }
}
