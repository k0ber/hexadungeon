﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapFabric : MonoBehaviour {

    public static MapFabric instance;

    public GameObject roomPrefab;
    public GameObject linePrefab;

    public Sprite currentRoomSprite;
    public Sprite exitRoomSprite;
    public Sprite visitedRoomSprite;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public GameObject CreateRoom(Room room) {
        GameObject roomGO = Instantiate(roomPrefab, new Vector2(room.x, room.y), Quaternion.identity);
        SpriteRenderer spriteRenderer = roomGO.GetComponent<SpriteRenderer>();
        switch (room.roomType) {
            case RoomType.UNKNOWN:
                break; // it's default prefab sprite
            case RoomType.CURRENT:
                spriteRenderer.sprite = currentRoomSprite;
                break;
            case RoomType.VISITED:
                spriteRenderer.sprite = visitedRoomSprite;
                break;
            case RoomType.EXIT:
                spriteRenderer.sprite = exitRoomSprite;
                break;
        }
        return roomGO;
    }

    public GameObject CreateLine(float x, float y) {
        return Instantiate(linePrefab, new Vector2(x, y), Quaternion.identity);
    }
}
