﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType {
    UNKNOWN,
    CURRENT,
    VISITED,
    EXIT
}
