﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils {

    public static void AddIfNotNull<T>(List<T> list, T value) {
        if (value != null)
            list.Add(value);
    }

    public static T MinBy<T>(List<T> list, Func<T, float> selector) {
        if (list.Count == 0) {
            return default(T);
        }
        float min = selector(list[0]);
        T minItem = list[0];
        for (int i = 1; i < list.Count; i++) {
            float itemValue = selector(list[i]);
            if (itemValue < min) {
                min = itemValue;
                minItem = list[i];
            }
        }
        return minItem;
    }

    public static T MaxBy<T>(List<T> list, Func<T, float> selector) {
        if (list.Count == 0) {
            return default(T);
        }
        float max = selector(list[0]);
        T maxItem = list[0];
        for (int i = 1; i < list.Count; i++) {
            float itemValue = selector(list[i]);
            if (itemValue > max) {
                max = itemValue;
                maxItem = list[i];
            }
        }
        return maxItem;
    }
}
